#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <unistd.h>

double pi = M_PI;
double Q= 0.0;
/* Q dient zum an/aus-schalten der Wärmequelle  Q(x,y)*/


double** arrayinit(double** T,int Nx,int Ny);
void ftcs(double **T, double **T_new, double Pe, double dx, double dy, double dt, int Nx, int N,double tfinal);
double q(int i, int j,double dx, double dy, double Pe);
double T_stern(int i, int j,double dx, double dy);

void aufgabe1(double Pe, double dx, double dy, double dt, int Nx, int Ny);
void aufgabe2(double Pe, double dx, double dy, double dt, int Nx, int Ny);
void aufgabe31(double Pe, double dx, double dy, double dt, int Nx, int Ny);
void aufgabe32(double Pe, double dx, double dy, double dt, int Nx, int Ny);
void aufgabe5(double Pe, double dx, double dy, double dt, int Nx, int Ny);

int main(int argc, char *argv[]){
	/*Initialisierung der Variablen für Gitterpunkte N,Zeitschrit dt, 
	 sowie Abstand der Gitterpunkt dx und dy und der Pe Zahl*/ 
	int N = 10;
	int Nx=N, Ny=N;
	double Pe = 2.0;
	double dx = 1.0/Nx;
	double dy = 1.0/Ny;
	double dt = 0.001;

	// Festlegung der Kommandozeilenargumente
	int a;
	char c;
	while ((c = getopt(argc, argv, "a:p:")) != -1){
		switch(c) {
			case 'a':
				a = atoi(optarg);
				break;
			case '?':
				printf("Fehler bei Kommandozeilenargumenten!\n");
				exit(0);
				break;
			default:
				abort();
		}
	}	 
	/*Fallunterscheidung darüber welche Aufgabe aufgerufen werden soll*/ 
	switch(a){
		case 1:aufgabe1(Pe,dx,dy,dt,Nx,Ny); break;
		case 2:aufgabe2(Pe,dx,dy,dt,Nx,Ny); break;
		case 31:aufgabe31(Pe,dx,dy,dt,Nx,Ny); break;
		case 32:aufgabe32(Pe,dx,dy,dt,Nx,Ny); break;
		case 5:aufgabe5(Pe,dx,dy,dt,Nx,Ny); break;
		default: break;
	}

	return 0;
	
}



double** arrayinit(double** T,int Nx,int Ny){
	/*Die Funktion dient zur Erzeugung eines Arrays mit Nx+1*Ny+1 Gitterpunkten,
	dabei werden zudem die Randbedingungen für t=0 realisiert*/
	T = (double **) malloc((Nx+1) * sizeof(double *));
	int i,j;
	double dx = 1.0/Nx;
	double dy = 1.0/Ny;
	for (i = 0; i <= Nx; i++){
		T[i] = (double *) malloc((Ny+1) * sizeof(double));
		for (j = 0; j <= Ny; j++){
			T[i][j] = dy*j;//Bedingung für Randbedingung bei t=0
		}
	}
	return T;
}

void ftcs(double **T, double **T_new, double Pe, double dx, double dy, double dt, int Nx, int Ny,double tfinal){
	/*In dieser Funktion ist die Integration von T_n auf T_n+1 
	 implementiert, hierfür wird zunächst eine For-Schleife 
	 Für die Zeit aufrufen */	
	
	int i, j;
	double t;
	for (t = 0; t < tfinal; t += dt) {
    	/*Für jeden Zeitschritt wird  für jeden Gitterpunkt 
    	 *der nächste Temperaturwert T_n+1 nach der in Aufgabe 1
    	 *hergeleiten Formelberechnet dafür werden 2 For-Schleifen verwendet*/    	
        for (i = 1; i < Nx; i++) {//x-Richtung
            for (j = 1; j < Ny; j++) {//y-Richtung
            	T_new[i][j]  = T[i][j];
            	T_new[i][j] += (dt/(dx*dx))*(T[i+1][j]-2.0*T[i][j]+T[i-1][j]); 
            	T_new[i][j] += (dt/(dy*dy))*(T[i][j+1]-2.0*T[i][j]+T[i][j-1]); 
            	T_new[i][j] -= dt*Pe*(pi*sin(2.0*pi*i*dx)*cos(pi*j*dy)*(T[i+1][j]-T[i-1][j])/(2.0*dx)); 	
            	T_new[i][j] += dt*Pe*(2.0*pi*cos(2.0*pi*i*dx)*sin(pi*j*dy)*(T[i][j+1]-T[i][j-1])/(2.0*dy)); 
            	//Quellterm arbeitet nur mit Q=1.0
            	T_new[i][j] += dt*Q*q(i,j,dx,dy,Pe);
            }
        }
        
        /*Für die Neumann Randbedingung wird mit einer 
        weiteren For-schleife über die Ränder summiert*/
        for (j = 1; j < Ny; j++){//y-Richtung       		
        		//unterer Rand
        		T_new[0][j]  = T[0][j];
            	T_new[0][j] += (dt/(dx*dx))*(T[1][j]-2.0*T[0][j]+T[1][j]); 
            	T_new[0][j] += (dt/(dy*dy))*(T[0][j+1]-2.0*T[0][j]+T[0][j-1]); 
            	T_new[0][j] += dt*Pe*(2.0*pi*cos(2.0*pi*i*dx)*sin(pi*j*dy)*(T[0][j+1]-T[0][j-1])/(2.0*dy)); 
            	//oberer Rand
				T_new[Nx][j]  = T[Nx][j];
            	T_new[Nx][j] += (dt/(dx*dx))*(T[Nx-1][j]-2.0*T[Nx][j]+T[Nx-1][j]); 
            	T_new[Nx][j] += (dt/(dy*dy))*(T[Nx][j+1]-2.0*T[Nx][j]+T[Nx][j-1]); 
            	T_new[Nx][j] += dt*Pe*(2.0*pi*cos(2.0*pi*i*dx)*sin(pi*j*dy)*(T[i][j+1]-T[i][j-1])/(2.0*dy)); 
                //Quellterm arbeitet nur mit Q=1.0		
        		T_new[0][j] += dt*Q*q(0,j,dx,dy,Pe);
        		T_new[Nx][j] += dt*Q*q(Nx,j,dx,dy,Pe);
        }
        //Array überschreiben 	
        for (i = 0; i <= Nx; i++) {
            for (j = 0; j <= Ny; j++) {
                T[i][j] = T_new[i][j];
            }
   	    }
	}

}

double q(int i, int j,double dx, double dy, double Pe){
	/*gibt den Quellterm Q(x,y) zurück*/
	double q;
	double x,y;
	x=i*dx;
	y=j*dy;
	q=-2*Pe*pi*sin(pi*y)*(pi*cos(pi*x)*cos(pi*x)*cos(pi*x)*cos(pi*y)+cos(2*pi*x));
	q+=2*pi*pi*cos(pi*x)*sin(pi*y);
	return q;
}

double T_x(int i, int j,double dx, double dy){
	/*gibt den Funktionswert T* aus Aufgabe 2 zurück*/
	return cos(pi*i*dx)*sin(pi*j*dy)+j*dy;
}



void aufgabe1(double Pe, double dx, double dy, double dt, int Nx, int Ny){

	int i,j;
	//Intetgraion bis tend = 1.0
    double tfinal = 1.0;
    //Erzeugen einer Ausgabedatei
    FILE *file;
    file = fopen("ausgabe.txt", "w+");
    
    //Initalisierung des Temperaturfelds	
	double **T=arrayinit(T,Nx,Ny);
	double **T_new=arrayinit(T_new,Nx,Ny);
	
    //Integration der Differentialgleichung	
	ftcs(T,T_new,Pe,dx,dy,dt,Nx,Ny,tfinal);

	//Ausgabe des Temperaturfelds in Datei "Ausgabe.txt"
    
    for (j = 0; j <= Ny; j++){
    	for (i = 0; i <= Nx; i++){
        	fprintf(file, "%f\t", T[i][j]);
        	}
        fprintf(file, "\n");
        }
    fclose(file);	
	}

void aufgabe2(double Pe, double dx, double dy, double dt, int Nx, int Ny){
	
	int i,j,N;
	//Integration bis tend=1
    double tend = 1.0 ;
    
    FILE *file;
    file = fopen("ausgabe.txt", "w+");
    
	double t;
	double ram, summe;
	//Quellterm einschalten
	Q=1.0;
	dt=0.001;
	/*Berechnung des Fehler für verschiedene N mit For-Schleife*/ 
	for(N=5;N<=40;N++){
		//Setze Nx und Ny auf das aktuelle N 
		Nx=N;
		Ny=N;
		/*Berechnung von dy,dy und dt, wobei dt so umgerechnet 
		wird, dass das CFL Kriterium weiterhin gilt*/
		ram=dt/(dx*dx);
		dx = 1.0/Nx;
		dy = 1.0/Ny;
		dt=ram*dx*dx;
		
		//Initialisierung des Temperaturfelds
		double **T=arrayinit(T,Nx,Ny);
		double **T_new=arrayinit(T_new,Nx,Ny);
		//Integration		
		ftcs(T,T_new,Pe,dx,dy,dt,Nx,Ny,tend);
		//Berechnung des Fehlers
		summe=0;
		for (i=0;i<=Nx;i++){
			for(j=0;j<=Ny;j++){
				summe+=(T_x(i,j,dx,dy)-T[i][j])*(T_x(i,j,dx,dy)-T[i][j]);
				}
		}
		summe=sqrt(summe);
		//Lösche Arrays aus dem speicher
		free(T);
		free(T_new);
		//Ausgabe von N und dem Fehler in Datei "Ausgabe.txt"		
        fprintf(file,"%i \t %f \n",N, summe);
    }
    fclose(file);	
    Q=0;
	
}

void aufgabe31(double Pe, double dx, double dy, double dt, int Nx, int Ny){
	
	
	FILE *file;
	file = fopen("ausgabe.txt", "w+");
	//Integration bis 1.0	
	double tend = 1.0;
	double t;
	int i;
	/*Nu wird für Pe = 1 und Pe =10 berechnet, daher ist 
	ein weiteres Temperaturfeld sowie Summationsvariable nötig*/
	
	double Nu1, Nu2;
	
	double **T=arrayinit(T,Nx,Ny);
	double **T2=arrayinit(T2,Nx,Ny);//Temperaturfeld für Pe = 10
	double **T_new=arrayinit(T_new,Nx,Ny);
	
	//Berechnung bis tend
	for(t=0;t<=tend;t+=dt){
		//Setze beide Nu auf 0 zurück
		Nu1 = 0;
		Nu2 = 0;
		//Integration über dx
		for(i=0;i<Nx;i++){
			//Berechnung der Ableitung in dy Richtung
			Nu1+=((-3.0/2.0*T[i+1][0]+2.0*T[i+1][1]-0.5*T[i+1][2])+(-3.0/2.0*T[i][0]+2.0*T[i][1]-0.5*T[i][2]))*(dx/dy*0.5);
			Nu2+=((-3.0/2.0*T2[i+1][0]+2.0*T2[i+1][1]-0.5*T2[i+1][2])+(-3.0/2.0*T2[i][0]+2.0*T2[i][1]-0.5*T2[i][2]))*(dx/dy*0.5);
		}
		/*Berechnung des Temperatufeldes bis zum nächsten Zeitschritt,
		daher wird tend=dt übergeben*/
		ftcs(T,T_new,Pe,dx,dy,dt,Nx,Ny,dt);
		ftcs(T2,T_new,Pe*5.0,dx,dy,dt,Nx,Ny,dt*0.1);
		//Ausgabe von t,Nu1, Nu2 in Datei "ausgabe.txt"
		fprintf(file, "%f \t %f \t %f\n",t, Nu1, Nu2);
	}
	//Ausgabe von Nu bei tend der Integration in die Konsole
	printf("Aufgabe31:\n Pe=2;Nu=%f \t Pe=10;Nu=%f\n",Nu1,Nu2);
	fclose(file);	
}

void aufgabe32(double Pe, double dx, double dy, double dt, int Nx, int Ny){
	FILE *file;
	file = fopen("ausgabe.txt", "w+");
	
	//Integration bis tend
	double tend = 1.0;
	double t;
	int i,j;
	double Nu;
	//Initalilsierung der Temperaturfelder
	double **T=arrayinit(T,Nx,Ny);
	double **T_new=arrayinit(T_new,Nx,Ny);
	//Berechnung für Pe im Intervall 0-10 mit For-Schleife
	for(Pe=0.0;Pe<=10.0;Pe+=0.1){
		//Integration bis tend
		ftcs(T,T_new,Pe,dx,dy,dt,Nx,Ny,tend);		
		//Berechnung von Nu 
		Nu = 0;
		for(i=0;i<Nx;i++){
			Nu+=((-3.0/2.0*T[i+1][0]+2.0*T[i+1][1]-0.5*T[i+1][2])+(-3.0/2.0*T[i][0]+2.0*T[i][1]-0.5*T[i][2]))*(dx/dy*0.5);
		}
		/*Anschließend wird das Temperaturfeld für den nächsten Pe-Wert
		auf die Anfangsbedingung zurückgesetzt*/
		for (i = 0; i <= Nx; i++) {
		    for (j = 0; j <= Ny; j++) {
				T[i][j]=dy*j;
			}
		}
		//Ausgabe von Nu in Abhängigkeit von Pe in "ausgabe.txt"		
		fprintf(file, "%f \t %f\n",Pe, Nu);
	}
	fclose(file);	
	
}

void aufgabe5(double Pe, double dx, double dy, double dt, int Nx, int Ny){
	
	int i,j;

	double summe;	
	double tend = 5.0;
	/* Tc wird als Vergleichsfeld bis tend integriert, für dt=0.001  
	und N=10 ist die Stabilität des Verfahrens gewährleistet */ 
	double **Tc = arrayinit(Tc,Nx,Ny);
	//Initalisierung des Temperaturfelds
	double **T=arrayinit(T,Nx,Ny);
	double **T_new=arrayinit(T_new,Nx,Ny);
	
	printf("Aufgabe5: \n");
	//Berechnung für Pe=0.1,1 und 10 
	for(Pe=0.1;Pe<=10;Pe*=10.0) {
		
		//Temperaturfeld Tc auf Anfangsbedingung zurücksetzen
		for (i = 0; i <= Nx; i++) {
			for (j = 0; j <= Ny; j++){
				Tc[i][j]=dy*j;
			}
		}	
		//Integration von Tc
		ftcs(Tc,T_new,Pe,dx,dy,0.001,Nx,Ny,tend);
		//Erhöhung von dt mit Forschleife
		for(dt=0.001;dt<0.1;dt+=0.00001){
			//Integration von T
			ftcs(T,T_new,Pe,dx,dy,dt,Nx,Ny,tend);
			//Fehler von T berechnen
			summe=0;
			for (i = 0; i < Nx; i++) {
				for (j = 0; j < Ny; j++) {
					summe+=(T[i][j]-Tc[i][j])*(T[i][j]-Tc[i][j]);
				}	
			}
			/*Abschließend muss das Temperaturfeld  T wieder zurück auf 
			die Anfangsbedingung gesetzt werden*/
			for (i = 0; i <= Nx; i++) {
				for (j = 0; j <= Ny; j++) {
					T[i][j]=dy*j;
				}
			}
			
			//Abbruchbedingung für dt
			if(summe>100000){
				printf("Pe:%f \t dt: %f \n",Pe, dt-0.00001);
				break;
			}
		}
				
	}
}



