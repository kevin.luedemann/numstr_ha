#wichtige Imports fuer das plotten
import matplotlib as mp
mp.use('TkAgg')
import matplotlib.pyplot as plt
#import aller Math funktionen
from pylab import *

#Wahl der Gittergroesse und erstellen des Mashgrids
NX	= 15
NY = 15
X,Y	= np.mgrid[0:1:NX*1j, 0:1:NY*1j]

#erzeigen des Geschwindigkeitsfeldes
vx	= pi*sin(2.0*pi*X)*cos(pi*Y)
vy	= -2.0*pi*cos(2.0*pi*X)*sin(pi*Y)

#plotten des Feldes
fig = plt.figure()
plt.quiver(X,Y,vx,vy)
plt.xlabel = "x"
plt.ylabel = "y"
plt.axis([-0.1,1.1,-0.1,1.1])

#speichern
fig.show()
plt.savefig("v0.png",dpi=200)
plt.close()
