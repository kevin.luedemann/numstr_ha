#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <cmath>
#include <string>
using namespace std;

extern "C"{
//	#include "nrutil.h"
//	#include "nrutil.c"
//	#include "gaussj.c"
}

//++++++++++++++++++++++++++++++++++++++++
//Parameter zum einstellen der Berechnung
char RUN_NAME[]	= "felder";
const int N_MAX	= 50;
double dt	= 0.0002;
int NX		= 30;
int NY		= NX;
double dx	= 1.0/(double)NX;
double dy	= 1.0/(double)NY;
//----------------------------------------

//++++++++++++++++++++++++++++++++++++++++
//Funktionsdeklarationen
void setze_IC(double **T);
void setze_N(int N);
void setze_IC_stat(double **T_stat);
void matrix_vektor_mul(double **A, double *b, int n);
void matrix_ober_mul(double **A, double *b, int n);
void matrix_print(double **M, int n);
void matrix_print_k(double **M, int n);
void vector_print(double *x, int n);
void vector_print_k(double *x, int n);
void vector_addition(double *x, double *b, int n);
void vector_subtraktion(double *x, double *b, int n);
void vector_copy(double *a, double *b, int n);
void vector_scalar(double *a, double w, int n);
void loese_vorwaerts(double **LD, double *x, int n);
double **build_matrix(double Pe, int n);
double *build_vector(double **T,  double Pe,int n);
void get_vector(double *v, double **T, double Pe, int n);
double vector_betrag(double *r, int n);
int SOR(double **M, double *b, int n, double toll, double ww, bool find_best);
void zeitschritt_explizit(double **T, double **T_cpy, double Pe, bool QUELLTERM);
void zeitschritt_implizit(double **T, double Pe);
double fehler_loesung(double **T, double **T_stat);
double Berechnung(double T_MAX, double Pe);
double Berechnung_implizit(double T_MAX, double Pe);
double Berechnung_plots(double T_MAX[], int schritte, double Pe, bool QUELLE);
double Berechnung_fehler(double T_MAX, double Pe, bool QUELLE);
int Berechnung_maxdt(int maxstep, double Pe, double tolleranz, FILE *ausgabe);
void plotten(double T_MAX[], int schritte, double Pe);
void plotten_k(double T_MAX, double Pe);
void ausgabe_feld(double **T, FILE *out, double zeit, double Pe);
//----------------------------------------
#define NR_END 1
#define FREE_ARG char*
double **dmatrix(long nrl, long nrh, long ncl, long nch)
/* allocate a double matrix with subscript range m[nrl..nrh][ncl..nch] */
{
   long i, nrow=nrh-nrl+1,ncol=nch-ncl+1;
   double **m;

   /* allocate pointers to rows */
   m=(double **) malloc((size_t)((nrow+NR_END)*sizeof(double*)));
   m += NR_END;
   m -= nrl;

   /* allocate rows and set pointers to them */
   m[nrl]=(double *) malloc((size_t)((nrow*ncol+NR_END)*sizeof(double)));
   m[nrl] += NR_END;
   m[nrl] -= ncl;

   for(i=nrl+1;i<=nrh;i++) m[i]=m[i-1]+ncol;

   /* return pointer to array of pointers to rows */
   return m;
}
void free_dmatrix(double **m, long nrl, long nrh, long ncl, long nch)
/* free a double matrix allocated by dmatrix() */
{
   free((FREE_ARG) (m[nrl]+ncl-NR_END));
   free((FREE_ARG) (m+nrl-NR_END));
}


int main(int argc, char *argv[]){
	//Aufgabe 6
	//test SOR mit bekannter Matrix und Vektor
	/*
	printf("Ausgabe 6 Gaus-Seidel Test\n");
	int nh = 1;
	double **M; M = dmatrix(0,nh,0,nh);
	double *b; b = (double *)malloc((nh+1)*sizeof(double));

	M[0][0] = 16.0;
	M[0][1] = 3.0;
	M[1][0] = 7.0;
	M[1][1] = -11.0;
	b[0]  = 11.0;
	b[1]  = 13.0;

	vector_print(b,nh);
	matrix_vektor_mul(M,b,nh);
	int steps = SOR(M,b,nh,1e-4,0.9,true);
	printf("#steps:%d\n",steps);
	vector_print(b,nh);
	printf("\n");
	free(b);
	free_dmatrix(M,0,nh,0,nh);
	*/
	printf("Ausgabe 6 implizit Euler\n");
	setze_N(30);
	dt = 100.0;
	
	Berechnung_implizit(0.0,10.0);
	plotten_k(dt,10.0);
	
	return 0;
}

//Funktion zum seten der IC
void setze_IC(double **T){
	int i,j;
	for(i=0;i<=NX;i++){
		for(j=0;j<=NY;j++){
			T[i][j]			= j*dy; 
		}
	}
}
void setze_N(int N){
	NX=N;	NY=NX;
	dx	= 1.0/(double)NX;
	dy	= 1.0/(double)NY;
}
void setze_IC_stat(double **T_stat){
	int i,j;
	for(i=0;i<=NX;i++){
		for(j=0;j<=NY;j++){
			T_stat[i][j]	= cos(M_PI*i*dx)*sin(M_PI*j*dy)+j*dy;
		}
	}
}

double Berechnung_implizit(double T_MAX, double Pe){
	double Fehler_berechung = 0.0;

	char ausgabe_name[50];
	sprintf(ausgabe_name,"%s_implizit_%.0e.dat",RUN_NAME,Pe);
	FILE *ausgabe;
	ausgabe = fopen(ausgabe_name, "w+");

	double **T;	T = dmatrix(0,NX,0,NY);
	setze_IC(T);

	double zeit = 0.0;
	while(zeit<=T_MAX){
		zeitschritt_implizit(T,Pe);
		zeit += dt;
	}

	ausgabe_feld(T,ausgabe,zeit,Pe);
	fclose(ausgabe);
	free_dmatrix(T,0,NX,0,NY);
	return Fehler_berechung;

}

//Funktion zur Ausgabe des Feldes in die Ausgabedatei mit der jeweiligen Zeit und der Peclet Zahl
//die Ausgabe erfolgt im Matrixformat, welches von gnuplot gelesen werden kann
void ausgabe_feld(double **T, FILE *out, double zeit, double Pe){
	fprintf(out,"#Zeit=%e\tPe=%f\n",zeit,Pe);
	int i,j;
	for(i=0;i<=NX;i++){
		for(j=0;j<=NY;j++){
			fprintf(out,"%e ",T[i][j]);
		}
		fprintf(out,"\n");
	}
	fprintf(out,"\n\n");
}

void zeitschritt_implizit(double **T, double Pe){
	double *T_vec;	
	double **M;
	M = build_matrix(Pe,NX);
//	matrix_print_k(M,(NX+1)*(NX+1));
	//printf("\n");
	T_vec = build_vector(T,Pe,NX);
	//matrix_print(T,NX);
	//printf("\n");
	//vector_print_k(T_vec,(NX-1)*(NX-1));
	//int steps = SOR(M,T_vec,(NX-1)*(NX-1)-1,1e-4,0.7,true);
	int steps = SOR(M,T_vec,(NX+1)*(NX+1)-1,1e-4,1.58,false);
	printf("#steps=%d\n",steps);
	get_vector(T_vec,T,Pe,NX);
	//matrix_print(T,NX);
	//printf("\n");
	free(T_vec);
	free_dmatrix(M,0,(NX+1)*(NX+1)-1,0,(NX+1)*(NX+1)-1);
}

int SOR(double **M, double *b, int n, double toll, double ww, bool find_best){
	int i,steps=0;
	double w = ww;
	double Fehler;
	double *x; x = (double *)malloc((n+2)*sizeof(double));
	double *r; r = (double *)malloc((n+2)*sizeof(double));
	double *x_alt; x_alt = (double *)malloc((n+2)*sizeof(double));

	if(find_best){
		char ausgabe_name[50];
		sprintf(ausgabe_name,"%s_w_analyse_%d_%.0e.dat",RUN_NAME,NX,toll);
		FILE *ausgabe;
		ausgabe = fopen(ausgabe_name, "w+");
		w = 0.01;
		while(w<=2.0){
		steps = 0;
		for(i=0;i<=n;i++){
				x[i] = 1.0;
		}
	
		vector_copy(x_alt,x,n);
		matrix_vektor_mul(M,x_alt,n);
		vector_subtraktion(x_alt,b,n);
		Fehler = vector_betrag(x_alt,n);
		while(Fehler>toll && steps < 1e4 && !isnan(Fehler) && !isinf(Fehler)){
			loese_vorwaerts(M,x_alt,n);
			vector_scalar(x_alt,w,n);
			vector_subtraktion(x,x_alt,n);
			//berechne rn
			vector_copy(x_alt,x,n);
			matrix_vektor_mul(M,x_alt,n);
			vector_subtraktion(x_alt,b,n);
			Fehler = vector_betrag(x_alt,n);
				
		steps++;	
		}
	
		printf("%.02e\t%03d\t%+e\n",w,steps,Fehler);
		fprintf(ausgabe,"%.02e\t%03d\t%+e\n",w,steps,Fehler);
		w += 0.01;
		}
		w = ww;
		fclose(ausgabe);
	}

	steps = 0;
	for(i=0;i<=n;i++){
			x[i] = 1.0;
	}

	vector_copy(x_alt,x,n);
	matrix_vektor_mul(M,x_alt,n);
	vector_subtraktion(x_alt,b,n);
	Fehler = vector_betrag(x_alt,n);
	while(Fehler>toll && steps < 1e4 && !isnan(Fehler) && !isinf(Fehler)){
		loese_vorwaerts(M,x_alt,n);
		vector_scalar(x_alt,w,n);
		vector_subtraktion(x,x_alt,n);
		//berechne rn
		vector_copy(x_alt,x,n);
		matrix_vektor_mul(M,x_alt,n);
		vector_subtraktion(x_alt,b,n);
		Fehler = vector_betrag(x_alt,n);
			
		steps++;	
	}
	
	printf("%.02e\t%03d\t%+e\n",w,steps,Fehler);
	for(i=0;i<=n;i++){
		b[i] = x[i];
	}
	//vector_copy(b,x,n);
	free(x_alt);
	free(x);
	free(r);
	
	return steps;
}

void matrix_ober_mul(double **A, double *b, int n){
	int i,j;
	double *x; x = (double *)malloc((n+1)*sizeof(double));
	for(i=0;i<=n;i++){
		x[i] = 0.0;
	}
	for(i=0;i<=n;i++){
		for(j=i+1;j<=n;j++){
			x[i] += A[i][j]*b[j];
		}
	}
	for(i=0;i<=n;i++){
		b[i] = x[i];
	}
	free(x);
}

void matrix_print(double **M, int n){
	int i,j;
	for(i=0;i<=n;i++){
		for(j=0;j<=n;j++){
			printf("%+.04e ",M[i][j]);
		}
		printf("\n");
	}
}

void matrix_print_k(double **M, int n){
	int i,j;
	for(i=0;i<n;i++){
		for(j=0;j<n;j++){
			printf("%+.04e ",M[i][j]);
		}
		printf("\n");
	}
}

void vector_print(double *x, int n){
	int i;
	for(i=0;i<=n;i++){
		printf("%+e\n",x[i]);
	}
}

void vector_print_k(double *x, int n){
	int i;
	for(i=0;i<n;i++){
		printf("%+e\n",x[i]);
	}
}

void loese_vorwaerts(double **LD, double *x, int n){
	int i,j;
	double *v; v = (double *)malloc((n+1)*sizeof(double));
	v[0] = x[0]/LD[0][0];
	for(i=1;i<=n;i++){
		v[i] = x[i];
		for(j=0;j<i;j++){
			v[i] -= v[j]*LD[i][j];
		}
		v[i] /= LD[i][i];
	}
	for(i=0;i<=n;i++){
		x[i] = v[i];
	}
	free(v);
}

void vector_scalar(double *a, double w, int n){
	int i;
	for(i=0;i<=n;i++){
		a[i] = a[i] * w;
	}
}

void vector_copy(double *a, double *b, int n){
	int i;
	for(i=0;i<=n;i++){
		a[i] = b[i];
	}
}

void vector_addition(double *x, double *b, int n){
	int i;
	for(i=0;i<=n;i++){
		x[i] += b[i];
	}
}

void vector_subtraktion(double *x, double *b, int n){
	int i;
	for(i=0;i<=n;i++){
		x[i] = x[i] - b[i];
	}
}

double vector_betrag(double *r, int n){
	int i;
	double summe = 0.0;
	for(i=0;i<=n;i++){
		summe += r[i]*r[i];
	}
	return sqrt(summe);
}

double **build_matrix(double Pe, int n){
	int i,j;
	double s = dt*Pe/(2.0*dx);
	double k = dt/(dx*dx);
	int na = n+1;
	int nn = na*na;
	double **M; M = dmatrix(0,nn-1,0,nn-1);
	for(i=0;i<nn;i++){
		for(j=0;j<0;j++){
			M[i][j] = 0.0;
		}
	}
	//Berechnung der Geschwindigkeits Matrizen
	double **v0_x; v0_x = dmatrix(0,n,0,n);
	double **v0_y; v0_y = dmatrix(0,n,0,n);
	for(i=0;i<=n;i++){
		for(j=0;j<=n;j++){
			v0_x[i][j] = M_PI*sin(2.0*M_PI*i*dx)*cos(M_PI*j*dy);
			v0_y[i][j] = -2.0*M_PI*cos(2.0*M_PI*i*dx)*sin(M_PI*j*dy);
		}
	}

	for(i=0;i<=n;i++){
		for(j=0;j<=n;j++){
			M[i*na+j][i*na+j] = 1.0+4.0*k;
			if(j==0 || j==n){
				M[i*na+j][i*na+j] = 1.0;
			}
			else{
				M[i*na+j][i*na+j-1] = -(s*v0_y[i][j]+k);
				M[i*na+j][i*na+j+1] =  (s*v0_y[i][j]-k);
			}
			if(i==0 && j != n && j!= 0){
				M[i*na+j][(i+1)*na+j] = -2.0*k;
			}
			else if(i==n && j != n && j!= 0){
				M[i*na+j][(i-1)*na+j] = -2.0*k;
			}
			else if(j!= 0 && j!= n){
				M[i*na+j][(i-1)*na+j] = -(s*v0_x[i][j]+k);
				M[i*na+j][(i+1)*na+j] =  (s*v0_x[i][j]-k);
			}
		}
	}

	free_dmatrix(v0_x,0,n,0,n);	
	free_dmatrix(v0_y,0,n,0,n);

	return M;
}

double *build_vector(double **T, double Pe, int n){
	int i,j;
	int nn = (n+1)*(n+1);
	double *v; v = (double *)malloc((nn-1)*sizeof(double));
	for(i=0;i<=n;i++){
		for(j=0;j<=n;j++){
			v[i*(n+1)+j] = T[i][j];
		}
	}
	return v;
}

void get_vector(double *v, double **T, double Pe, int n){
	int i,j;
	int nn = (n+1)*(n+1);
	for(i=0;i<=n;i++){
		for(j=0;j<=n;j++){
			T[i][j] = v[i*(n+1)+j]; 
		}
	}
}

void plotten(double T_MAX[], int schritte, double Pe){
	int weiter = 0;
	char bash_ausgabe[100];
	while(weiter<schritte){
		sprintf(bash_ausgabe,"gnuplot -e \"TINDEX=%i ; zeit=%f ; PEINDEX=%.0e ; RUN_NAME='%s_implizit' ; NX=%d\" temperatur.gp",weiter,T_MAX[weiter],Pe,RUN_NAME,NX);
		//sprintf(bash_ausgabe,"echo -e \"TINDEX=%i ; zeit=%f ; PEINDEX=%d ; RUN_NAME=%s\"",weiter,T_MAX[weiter],index,RUN_NAME);
		//printf(bash_ausgabe);
		//printf("\n");
		system(bash_ausgabe);
		weiter++;
	}
}

void plotten_k(double T_MAX, double Pe){
	char bash_ausgabe[100];
	sprintf(bash_ausgabe,"gnuplot -e \"TINDEX=%i ; zeit=%f ; PEINDEX=%.0e ; RUN_NAME='%s_implizit' ; NX=%d\" temperatur.gp",0,T_MAX,Pe,RUN_NAME,NX);
	//sprintf(bash_ausgabe,"echo -e \"TINDEX=%i ; zeit=%f ; PEINDEX=%d ; RUN_NAME=%s\"",weiter,T_MAX[weiter],index,RUN_NAME);
	//printf(bash_ausgabe);
	//printf("\n");
	system(bash_ausgabe);
}

void matrix_vektor_mul(double **A, double *b, int n){
	int i,j;
	double *x; x = (double *)malloc((n+1)*sizeof(double));
	for(i=0;i<=n;i++){
		x[i] = 0.0;
	}
	for(i=0;i<=n;i++){
		for(j=0;j<=n;j++){
			x[i] += A[i][j]*b[j];
		}
	}
	for(i=0;i<=n;i++){
		b[i] = x[i];
	}
	free(x);
}

