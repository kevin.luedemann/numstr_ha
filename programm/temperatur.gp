reset

set terminal png 

outputname=sprintf("T_%s_%.0e_%.0e.png",RUN_NAME,zeit,PEINDEX)
inputname=sprintf("%s_%.0e.dat",RUN_NAME,PEINDEX);
set xlabel "x"
set ylabel "y"

set title sprintf("Pe=%.1f",PEINDEX)

set output outputname
p inputname index TINDEX u ($2/NX):($1/NX):3 matrix with image t sprintf("T(t=%f)",zeit)
set output
