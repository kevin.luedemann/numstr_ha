reset

inputname="fehler_quellterm.dat"

set fit errorvariables
set fit logfile "fehler_quellterm.log"

f(x)=a/x+b
fit f(x) inputname index 0 using 1:2 via a,b 

set terminal epslatex color

set xlabel "$N_x=N_y$"
set ylabel "$\\sigma$"
set output "quellterm_fehler.tex"
p inputname u 1:2 w lp t "Berechneter Fehler", f(x) t sprintf("$%.3f(%.3f)/N%+.4f(%.4f)$",a,a_err,b,b_err)
set output
!epstopdf quellterm_fehler.eps

set ylabel "$\\Delta t$"
set logscale y
set output "dt_krit.tex"
p "dt_min.dat" index 0 u 3:1 w lp t "Pe=0.1", "" index 1 u 3:1 w lp t "Pe=1.0", "" index 2 u 3:1 w lp t "Pe=10"
set output
!epstopdf dt_krit.eps
unset logscale y
!cp dt_krit.* ../
