reset

stats[0.1:1.8] "felder_w_analyse_30_1e-04.dat" u 1:2

set terminal epslatex color
set xlabel "$\\omega$"
set ylabel "Schritte"
set output "minimum_w.tex"
p[0.1:1.8] "felder_w_analyse_30_1e-04.dat" u 1:2 w lp t "Iterationsschritte", STATS_min_y t sprintf("Minimum: %d",STATS_min_y)
set output
!epstopdf minimum_w.eps
#pause -1
