//notwendigke includes
#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <cmath>
#include <string>
using namespace std;

//++++++++++++++++++++++++++++++++++++++++
//Parameter zum einstellen der Berechnung
char RUN_NAME[]	= "felder";
//das Groesste N, das bei Iterationen verwendet wird
const int N_MAX	= 50;
double dt	= 0.0002;
int NX		= 30;
int NY		= NX;
double dx	= 1.0/(double)NX;
double dy	= 1.0/(double)NY;
//----------------------------------------

//++++++++++++++++++++++++++++++++++++++++
//Funktionsdeklarationen
void setze_IC(double **T);
void setze_N(int N);
void setze_IC_stat(double **T_stat);
void zeitschritt_explizit(double **T, double **T_cpy, double Pe, bool QUELLTERM);
double fehler_loesung(double **T, double **T_stat);
double Berechnung(double T_MAX, double Pe);
double Berechnung_plots(double T_MAX[], int schritte, double Pe, bool QUELLE);
double Berechnung_fehler(double T_MAX, double Pe, bool QUELLE);
int Berechnung_maxdt(int maxstep, double Pe, double tolleranz, FILE *ausgabe);
void plotten(double T_MAX[], int schritte, double Pe);
void ausgabe_feld(double **T, FILE *out, double zeit, double Pe);
//----------------------------------------
//Im Programm wird zur Erstellung von Matrizen die Funktion aus 
//"Numerical Recipes in c"
//Second Edition
//Cambridge University Press
//ISBN 0521431085
//verwendet
#define NR_END 1
#define FREE_ARG char*
double **dmatrix(long nrl, long nrh, long ncl, long nch)
/* allocate a double matrix with subscript range m[nrl..nrh][ncl..nch] */
{
   long i, nrow=nrh-nrl+1,ncol=nch-ncl+1;
   double **m;

   /* allocate pointers to rows */
   m=(double **) malloc((size_t)((nrow+NR_END)*sizeof(double*)));
   m += NR_END;
   m -= nrl;

   /* allocate rows and set pointers to them */
   m[nrl]=(double *) malloc((size_t)((nrow*ncol+NR_END)*sizeof(double)));
   m[nrl] += NR_END;
   m[nrl] -= ncl;

   for(i=nrl+1;i<=nrh;i++) m[i]=m[i-1]+ncol;

   /* return pointer to array of pointers to rows */
   return m;
}
void free_dmatrix(double **m, long nrl, long nrh, long ncl, long nch)
/* free a double matrix allocated by dmatrix() */
{
   free((FREE_ARG) (m[nrl]+ncl-NR_END));
   free((FREE_ARG) (m+nrl-NR_END));
}


int main(int argc, char *argv[]){
	double Fehler_berechung;

	//Loesung der Aufgabe 3
	FILE *ausgabe_fehler;
	ausgabe_fehler = fopen("fehler_quellterm.dat","w+");
	//Zeitschritt ausreichend fuer das Maximale N
	//bei neuer Wahl von N_MAX muss das hier beachtet werden
	dt = 0.0001;
	//Iteration durch die verschiedenen Aufloesung
	for(NX=2;NX<=N_MAX;NX++){
		setze_N(NX);
		//starten der Berechung mit T_MAX=1.0 und Pe=2.0
		Fehler_berechung = Berechnung_fehler(1.0,2.0,true);
		fprintf(ausgabe_fehler,"%d\t%+e\n",NX,Fehler_berechung);
	}
	fclose(ausgabe_fehler);
	system("gnuplot fehler_fit.gp");

	//Plotten der Ergebnisse fuer die Aufgabe 4
	printf("\n\n");
	//Reihe an Zeitpunkte fuer die Ausgabe
	double T_MAX[]	= {0.005,0.05,0.5};
	//Anzahl der Zeitpunkte
	int schritte 	= (int)(sizeof(T_MAX)/sizeof(double));
	dt = 0.0002;
	setze_N(30);
	//Peclet Zahlen fuer die Berechnung
	double Pe[]={2.0,10.0};
	int i_berechung;
	for(i_berechung=0;i_berechung<(int)(sizeof(Pe)/sizeof(double));i_berechung++){
		Fehler_berechung = Berechnung_plots(T_MAX,schritte,Pe[i_berechung],false);
		plotten(T_MAX,schritte,Pe[i_berechung]);
		//Ausgabe der Fehler der Berechnung fuer die Einzehlen Pe Werte am Ende der Rechnugn
		printf("%+e\t%+e\t%e\n",Pe[i_berechung],T_MAX[schritte-1],Fehler_berechung);
	}
	//Aufgabe 5
	FILE *ausgabe;
	ausgabe = fopen("dt_min.dat","w+");
	int steps;
	//Iteration uber viele N um das verhalten des Zeitschrittes zu erkennen
	//ausfuehren des Schiessverfahren fuer Pe=0.1
	for(NX=2;NX<=N_MAX;NX++){
		setze_N(NX);
		steps = Berechnung_maxdt(1e4,0.1,1e-9,ausgabe);
	}
	printf("\n\n");
	fprintf(ausgabe,"\n\n");
	//ausfuehren des Schiessverfahren fuer Pe=1.0
	for(NX=2;NX<=N_MAX;NX++){
		setze_N(NX);
		steps = Berechnung_maxdt(1e4,1.0,1e-9,ausgabe);
	}
	printf("\n\n");
	fprintf(ausgabe,"\n\n");
	//ausfuehren des Schiessverfahren fuer Pe=10
	for(NX=2;NX<=N_MAX;NX++){
		setze_N(NX);
		steps = Berechnung_maxdt(1e4,10.0,1e-9,ausgabe);
	}
	printf("\n\n");
	fprintf(ausgabe,"\n\n");
	fclose(ausgabe);
	return 0;
}

//Funktion zum seten der IC in der Temperatur
void setze_IC(double **T){
	int i,j;
	for(i=0;i<=NX;i++){
		for(j=0;j<=NY;j++){
			T[i][j]			= j*dy; 
		}
	}
}
//Funktion zu setzen der N und der dx Werte
void setze_N(int N){
	NX=N;	NY=NX;
	dx	= 1.0/(double)NX;
	dy	= 1.0/(double)NY;
}
//Funktion zum berechnen der Stationaeren Loesung fuer den Falls der Quellstroeung
void setze_IC_stat(double **T_stat){
	int i,j;
	for(i=0;i<=NX;i++){
		for(j=0;j<=NY;j++){
			T_stat[i][j]	= cos(M_PI*i*dx)*sin(M_PI*j*dy)+j*dy;
		}
	}
}

double Berechnung(double T_MAX, double Pe){
//Funktion, die einfach nur iteriert und am ende den Fehler am letzten Zeitschritt weidergibt
//der Fehler berechnet sich aus dem Abstand zum vorherigen Zeitschritt
	double Fehler_berechung = 0.0;

	double **T;	T = dmatrix(0,NX,0,NY);
	double **T_cpy; T_cpy = dmatrix(0,NX,0,NY);
	setze_IC(T);

	double zeit = 0.0;
	while(zeit<=T_MAX){
		zeitschritt_explizit(T,T_cpy,Pe,false);
		zeit += dt;
	}
	Fehler_berechung = fehler_loesung(T,T_cpy);
	free_dmatrix(T,0,NX,0,NY);
	free_dmatrix(T_cpy,0,NX,0,NY);
	return Fehler_berechung;
}

double Berechnung_plots(double T_MAX[], int schritte, double Pe, bool QUELLE){
//Funktion zum erstellen der Plots
//anhand des T_MAX array wird die Simmulation zu festen Zeitpunkten unterbrochen
//anschliessend wird das aktuelle Ergebnis gespeichert
//die Ausgabe wird anhand der paramter und eines speziellen namens gesetzt
	double Fehler_berechung = 0.0;
	char ausgabe_name[50];
	sprintf(ausgabe_name,"%s_explizit_%.0e.dat",RUN_NAME,Pe);
	FILE *ausgabe;
	ausgabe = fopen(ausgabe_name, "w+");

	double **T;	T = dmatrix(0,NX,0,NY);
	double **T_cpy; T_cpy = dmatrix(0,NX,0,NY);
	setze_IC(T);

	double zeit = 0.0;
	int weiter = 0; //Abbruchbedingung der Schleife
	//die Schleife wird beendet, sobald alles wichtigen Zeitpunkte erfasst wurden
	while(weiter<schritte){
		zeitschritt_explizit(T,T_cpy,Pe,QUELLE);
		//hier wird das programm unterbrochen
		if(zeit >= T_MAX[weiter] && zeit < T_MAX[weiter]+dt){
			ausgabe_feld(T,ausgabe,zeit,Pe);
			Fehler_berechung = fehler_loesung(T,T_cpy);
			weiter++;
		}
		zeit += dt;
	}
	fclose(ausgabe);
	free_dmatrix(T,0,NX,0,NY);
	free_dmatrix(T_cpy,0,NX,0,NY);
	return Fehler_berechung;
}

double Berechnung_fehler(double T_MAX, double Pe, bool QUELLE){
//Funktion zum bestimmen, ob die Berechnung gegen die Stationaere Loesung konvergiert
//hierzu wird nach dem T_MAX erreicht wurde der Fehler zur Stationaeren Loesung berechnet
	double Fehler_berechung = 0.0;

	double **T;	T = dmatrix(0,NX,0,NY);
	double **T_cpy; T_cpy = dmatrix(0,NX,0,NY);
	double **T_stat; T_stat = dmatrix(0,NX,0,NY);
	setze_IC(T);
	setze_IC_stat(T_stat);

	double zeit = 0.0;
	while(zeit<T_MAX){
		zeitschritt_explizit(T,T_cpy,Pe,QUELLE);
		zeit += dt;
	}
	//printf("#%d\t%e\n",NX,fehler_loesung(T,T_cpy));
	Fehler_berechung = fehler_loesung(T,T_stat);
	free_dmatrix(T,0,NX,0,NY);
	free_dmatrix(T_cpy,0,NX,0,NY);
	free_dmatrix(T_stat,0,NX,0,NY);
	return Fehler_berechung;
}

int Berechnung_maxdt(int maxstep, double Pe, double tolleranz, FILE *ausgabe){
//Funktion zum bestimmen von dem maximalen dt
//es wird mit einem dt begonnen und jeh nach dem, ob das Verfahren Konvergiert oder
//divergiert, wird der Zeitschrit groesser oder kleiner, aber immer in kleiner werdenden Schritten
//die Genauigkeit kann Eingestellt werden
	double Fehler_berechung = 0.0;
	int step = 0;

	dt=0.1;
	double intervall=dt-0.0;
	while(step<maxstep && intervall>tolleranz){
		Fehler_berechung = Berechnung(5.0,Pe);
		if(Fehler_berechung>tolleranz || isnan(Fehler_berechung) || isinf(Fehler_berechung)){
			dt -= intervall/2.0;
		}
		else{
			dt += intervall/2.0;
		}
		intervall /= 2.0;
		step++;
	}
	printf("%e\t%d\t%d\t%.0e\n",dt,step,NX,Fehler_berechung);
	fprintf(ausgabe,"%e\t%d\t%d\t%.0e\n",dt,step,NX,Fehler_berechung);
	return step;
}

//Funktion zur Ausgabe des Feldes in die Ausgabedatei mit der jeweiligen Zeit und der Peclet Zahl
//die Ausgabe erfolgt im Matrixformat, welches von gnuplot gelesen werden kann
void ausgabe_feld(double **T, FILE *out, double zeit, double Pe){
	fprintf(out,"#Zeit=%e\tPe=%f\n",zeit,Pe);
	int i,j;
	for(i=0;i<=NX;i++){
		for(j=0;j<=NY;j++){
			fprintf(out,"%e ",T[i][j]);
		}
		fprintf(out,"\n");
	}
	fprintf(out,"\n\n");
}

void zeitschritt_explizit(double **T, double **T_cpy, double Pe, bool QUELLTERM){
//Funktion, die den Zeitschritt ausfuehrt
//Die Matrix T wird hierbei mit dem neuen Zeitschritt ueberschrieben
	int i,j;
	//Kopiere das Array in temp
	for(i=0;i<=NX;i++){
		for(j=0;j<=NY;j++){
			T_cpy[i][j] = T[i][j];
		}
	}
	//fuehre den Zeitschritt aus
	for(i=1;i<NX;i++){
		for(j=1;j<NY;j++){
			//Advektionsterme
			T[i][j] -= dt/(2.0*dx)*Pe*M_PI*sin(2.0*M_PI*i*dx)*cos(M_PI*j*dy)*(T_cpy[i+1][j]-T_cpy[i-1][j]);
			T[i][j] += dt/(2.0*dy)*Pe*2.0*M_PI*cos(2.0*M_PI*i*dx)*sin(M_PI*j*dy)*(T_cpy[i][j+1]-T_cpy[i][j-1]);
			//Diffusionsterme
			T[i][j] += dt/(dx*dx)*(T_cpy[i+1][j]-2.0*T_cpy[i][j]+T_cpy[i-1][j]);
			T[i][j] += dt/(dy*dy)*(T_cpy[i][j+1]-2.0*T_cpy[i][j]+T_cpy[i][j-1]);
			//Bei einem Quellterm wird dieser mit einberechnet
			if(QUELLTERM){
			T[i][j] -= dt*2.0*Pe*M_PI*sin(M_PI*j*dy)*(M_PI*cos(M_PI*i*dx)*cos(M_PI*i*dx)*cos(M_PI*i*dx)*cos(M_PI*j*dy)+cos(2.0*M_PI*i*dx));
			T[i][j] += dt*2.0*M_PI*M_PI*cos(M_PI*i*dx)*sin(M_PI*j*dy);
			}
		}
	}
	//fuer die von Neumann Raender wird eine eigene Schleife verwendet
	for(j=1;j<NY;j++){
			//Advektionsterm der Raender
			T[0][j] += dt/(2.0*dy)*Pe*2.0*M_PI*cos(2.0*M_PI*i*dx)*sin(M_PI*j*dy)*(T_cpy[0][j+1]-T_cpy[0][j-1]);
			//Diffusionsterm der Raender
			T[0][j] += dt/(dx*dx)*(T_cpy[1][j]-2.0*T_cpy[0][j]+T_cpy[1][j]);
			T[0][j] += dt/(dy*dy)*(T_cpy[0][j+1]-2.0*T_cpy[0][j]+T_cpy[0][j-1]); 
			//Bei einem Quellterm wird dieser mit einberechnet
			if(QUELLTERM){
			i=0;
			T[0][j] -= dt*2.0*Pe*M_PI*sin(M_PI*j*dy)*(M_PI*cos(M_PI*i*dx)*cos(M_PI*i*dx)*cos(M_PI*i*dx)*cos(M_PI*j*dy)+cos(2.0*M_PI*i*dx));
			T[0][j] += dt*2.0*M_PI*M_PI*cos(M_PI*i*dx)*sin(M_PI*j*dy);
			}
			//Advektionsterm
			T[NX][j] +=	dt/(2.0*dy)*Pe*2.0*M_PI*cos(2.0*M_PI*i*dx)*sin(M_PI*j*dy)*(T_cpy[NX][j+1]-T_cpy[NX][j-1]);
			//Diffusionsterm
			T[NX][j] += dt/(dx*dx)*(T_cpy[NX-1][j]-2.0*T_cpy[NX][j]+T_cpy[NX-1][j]);
			T[NX][j] += dt/(dy*dy)*(T_cpy[NX][j+1]-2.0*T_cpy[NX][j]+T_cpy[NX][j-1]);
			//Bei einem Quellterm wird dieser mit einberechnet
			if(QUELLTERM){
			i=NX;
			T[NX][j] -= dt*2.0*Pe*M_PI*sin(M_PI*j*dy)*(M_PI*cos(M_PI*i*dx)*cos(M_PI*i*dx)*cos(M_PI*i*dx)*cos(M_PI*j*dy)+cos(2.0*M_PI*i*dx));
			T[NX][j] += dt*2.0*M_PI*M_PI*cos(M_PI*i*dx)*sin(M_PI*j*dy);
			}
	}
}

double fehler_loesung(double **T, double **T_stat){
//Funktion zum Berechnen des Fehlers nach der Formel, wie in der Dokumentation angegeben
	int i,j;
	double summe = 0.0;
	for(i=0;i<=NX;i++){
		for(j=0;j<=NY;j++){
			summe += (T[i][j]-T_stat[i][j])*(T[i][j]-T_stat[i][j]);
		}
	}
	return sqrt(summe);
}

void plotten(double T_MAX[], int schritte, double Pe){
//Funktion zum ausfuehren der Plott funktion mit dem gnupkit script
//das Programm wird in der Basch ausgefuehrt, die das programm startet
	int weiter = 0;
	char bash_ausgabe[100];
	while(weiter<schritte){
		sprintf(bash_ausgabe,"gnuplot -e \"TINDEX=%i ; zeit=%f ; PEINDEX=%.0e ; RUN_NAME='%s_explizit' ; NX=%d\" temperatur.gp",weiter,T_MAX[weiter],Pe,RUN_NAME,NX);
		system(bash_ausgabe);
		weiter++;
	}
}

