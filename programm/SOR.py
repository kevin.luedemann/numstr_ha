import numpy as np
import numpy.linalg as nl
import scipy as sp
import scipy.linalg as sl
from pylab import *

def loese_vorw(M,b,n):
	v = np.zeros(n)
	v[0] = b[0]/M[0][0]
	for i in range(1,n):
		v[i] = b[i]
		for j in range(i):
			v[i] -= v[j]*M[i][j]
		v[i] /= M[i][i]
	return v

def mul_upper(M,b,n):
	v = np.zeros(n)
	for i in range(n):
		for j in range(i+1,n):
			v[i] += M[i][j]*b[i]
	return v
		

n = 2
w = 1.0

M = np.ones((n,n))
b = np.ones(n)

for i in range(n):
	b[i] = (i+1)
	for j in range(n):
		M[i][j] = i*n+j+1
"""
M[0][0] = 16.0
M[0][1] = 3.0
M[1][0] = 7.0
M[1][1] = -11.0
b[0]	= 11.0
b[1]	= 13.0
"""
print "M"
print M
print "b"
print b

#print loese_vorw(M,b,n)
#print mul_upper(M,b,n)

x = np.ones(n)
print x
x_alt = x

r = np.dot(M,x_alt)
r -= b
Fehler = nl.norm(r)
steps = 0
while(Fehler > 1e-5 and steps < 2e1):
	x_alt = loese_vorw(M,r,n)
	x_alt *= w
	x -= x_alt
	x_alt = x
	r = np.dot(M,x_alt)-b
	Fehler = nl.norm(r)
	print steps,Fehler
	steps += 1

print x	
print sl.solve(M,b)
